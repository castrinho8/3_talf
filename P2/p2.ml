(* TALF, PRACTICA 2: Gramáticas independientes del contexto, forma normal de Chomsky y algoritmo CYK
		Pablo Castro Valiño (pablo.castro1@udc.es)
		Marcos Chavarría Teijeiro (marcos.chavarria@udc.es)
*)


(*EJERCICIO 1*)


let es_un_terminal = function 
[] -> raise (Failure "son_dos_no_terminales: regla no valida!!")
	| (Terminal _)::[] -> true
	| _ -> false;;
	
let son_dos_no_terminales = function
		[] -> raise (Failure "son_dos_no_terminales: regla no valida!!")
	| (No_terminal _)::(No_terminal _)::[] -> true
	| _ -> false;; 

let es_FNC g = let Gic (_,_,Conjunto lr,_) = g in 
	let rec aux = function 
		[] -> true
	| l::h -> let Regla_gic (_,d) = l in
		(es_un_terminal(d) || son_dos_no_terminales(d)) && (aux h)
	in aux lr;;


	
(*EJERCICIO 2*)

type elemento = Elemento of 
		(regla_gic  * 
		(int*int) *
		(int*int));;

type celda = Celda of (elemento list);;

type tabla_CYK = Tabla of (
		celda list *
		int);;
	

let ancho t = let Tabla(_,l) = t in l;;
	
let celdas t = let Tabla(c,_) = t in c;;
		
let coordenada ancho x y = 
	if (y <= 0 || y > ancho || x <= 0 || x > ancho - y + 1)	
		then raise (Failure "coordenada")
		else 	if y = 1
				then x
				else 	let rec aux s fi =
 							if fi = y
							then s + x
							else aux (s + ancho - fi + 1) (fi + 1)
						in aux ancho 2;;
						
(*Funcion que devuelve una celda de la tabla*)		
let get t x y = 
	let Tabla(l,a) = t
	in List.nth l ((coordenada a x y) - 1);;
	
(*Funcion que devuelve un conjunto de simbolos de una celda*)
let getSimb t x y = 
	let Celda l = get t x y
	in conjunto_of_list (List.map (function Elemento(Regla_gic (s,_),_,_) -> s ) l);;
	

let anhadirElemento e t x y = 
	let rec aux li s = function 
		[] -> raise (Failure "anhdirElemento")
		| h::tail -> 	if s == 1
							then 	let Celda ls = h
									in (List.rev li) @ ((Celda (e::ls))::tail)
							else aux (h::li) (s-1) tail
	in let Tabla(ca,a) = t 
	in Tabla(aux [] (coordenada a x y) ca, a);;
	
(* Funcion que añade a una tabla una lista de elementos en una fila y columna determinadas *)	
let anhadirListaElementos lista tabla x y =
	let rec aux tab = function
			[] -> tab
		| h::t -> aux (anhadirElemento h tab x y) t
	in aux tabla lista;;



(*EJERCICIO 3*)	
	
(*Devuelve una tabla vacia del tamaño pasado por parametro.*)	
let inicializa_tabla n = 
	let rec aux i l =
		if i = (coordenada n 1 n)
		then Tabla((Celda([]))::l,n)
		else aux (i + 1) ((Celda([]))::l)
	in aux 1 [];;

(*Une una lista de listas en una lista respetando el orden [[1;2][3;4]] -> [1;2;3;4]  *)
let join l = 
	let rec aux lf = function 
			[] -> List.rev lf
		| h::t -> aux ((List.rev h) @ lf) t
	in aux [] l;;

(*Devuelve una lista de no terminales que derivan la cadena pasada por parametro directamente 
	con el conjunto de reglas pasado por parametro.*)
let analizar_reglas reglas cadena =
	let rec aux lf = function 
			[] -> lf
		| (Regla_gic (nt,cad))::t -> 	if cad = cadena
												then aux ((Regla_gic (nt,cad))::lf) t
												else aux lf t
	in aux [] (list_of_conjunto reglas);;

(*Añade a la tabla pasada por parametro en la fila y columna pasadas por parametro los nt de la lista con las procedencias t1 y t2*)	
let anhadirListaReg tabla lista_reg t1 t2 x y	= 	
	let lista_elementos = (List.map (function e -> Elemento(e,t1,t2)) lista_reg)
	in anhadirListaElementos lista_elementos tabla x y;;

	
let parse_CYK c g = let Gic(_,_,reglas,i) = g in
	if (List.length(c) < 1) ||  (not (es_FNC g))
	then raise (Failure "parse_CYK")
	else let rec aux j tabla = 
				if j = 1
				then 	let rec rellenar_primera tab i = 	
							if i > List.length(c)
							then tab 
							else 	let lista_reg = analizar_reglas reglas [(List.nth c (i-1))]
									in rellenar_primera (anhadirListaReg tab lista_reg (0,0) (0,0) i 1) (i+1)
						in aux (j + 1) (rellenar_primera tabla 1)
						
				else 	if j = List.length(c) + 1
						then tabla
						else 	let rec recorre_fila i tabl =
									if i = List.length(c) - j + 2
									then tabl
									else 	let rec analiza_celda tabla k = 
												if k = j
												then tabla
												else 	let lista_cadenas = List.map (function (a,b) -> [a;b]) (list_of_conjunto (cartesiano (getSimb tabla i k) (getSimb tabla (k+i) (j-k)) ))
														in let lista_reg = (join (List.map (analizar_reglas reglas) lista_cadenas))
														in analiza_celda (anhadirListaReg tabla lista_reg (i,k) (k+i,j-k) i j) (k + 1)
											in recorre_fila (i + 1) (analiza_celda tabl 1)
								in aux (j+1) (recorre_fila 1 tabla) 
			in aux 1 (inicializa_tabla (List.length c));;
			
			
(*Ejercicio 4*)


let arbol_CYK x y tabla = 

	(*Devuelve los subarboles de una celda correspondientes a un determinado No Terminal. *)
	let rec subArb_NT celda no_terminal =
		(let Celda(l) = celda
		in let lista_elementos = List.filter (function Elemento((Regla_gic (nt,_)),_,_) -> (nt = no_terminal)) l

		in let rec aux lf = (function 
				[] -> lf
		 		|	(Elemento((Regla_gic (nt,cad)),t1,t2))::cola -> if (t1,t2) = ((0,0),(0,0))
																					then 
																						try 
																							let [(Terminal b)] = cad
																							and No_terminal a = nt
																							in aux (("(" ^ a ^ " " ^ b ^ ")")::lf) cola
																						with
																							Match_failure _  -> raise (Failure "subArb_NT: Un elemento sin origen contiene No_Terminales o una regla no esta en FNC.") 
																					else 		
																						try 
																							let [nt1;nt2] = cad
																						   and No_terminal raiz = nt
																							and ((x1,x2),(y1,y2)) = (t1,t2)
																						   in let (l1,l2) = ((subArb_NT (get tabla x1 x2) nt1), (subArb_NT (get tabla y1 y2) nt2)) (*Llamada recursiva.*)
																								(* Devuelve todos los strings del subarbol recibiendo como parametros una lista de pares subarbol izq., subarbol der.*)
																							in let rec aux2 lstring = function
																									[] -> lstring
																								| h::t -> 	let (a,b) = h 
																							 					in aux2 (("(" ^ raiz ^ " " ^ a ^ " " ^ b ^ ")")::lstring) t
																							 	in let lista = list_of_conjunto (cartesiano (conjunto_of_list l1) (conjunto_of_list l2))
																								in aux ( (aux2 [] lista) @ lf) cola
																						with
																								Match_failure _ -> raise (Failure "subArb_NT: Un elemento con origen no contiene dos No terminales.") )
			in aux [] lista_elementos)
	in join (List.map (subArb_NT (get tabla x y)) (list_of_conjunto (getSimb tabla x y) ) );;
					
			
					