(*EJERCICIO 1*)
let mapdoble f g l = 
	let rec aux f g l n =
		let h::t = l in 
			if ((n mod 2) = 0) then 
				if t = [] then g(h)::[] else g(h)::aux f g t (n+1)
			else 
				if t = [] then f(h)::[] else f(h)::aux f g t (n+1)
	in 	
		if l = [] then raise (Invalid_argument "Lista vacia")
		else aux f g l 1;;

(*Tipo: *) mapdoble : ('a -> 'b) -> ('a -> 'b) -> 'a list -> 'b list = <fun>

(*Error: This expression has type string but an expression was expected of type
        int*)
(*Tipo: *) - : ('_a -> int) -> '_a list -> int list = <fun>


(*EJERCICIO 2*)
let primero_que_cumple f l =
	let rec aux f l =
		let h::t = l in
			if f(h) then h
			else 
				if t = [] then raise (Failure "No existe ningun elemento que lo cumpla")
				else aux f t
	in 
		if l = [] then raise (Invalid_argument "Lista vacia")
		else aux f l;;

(*Tipo: *) primero_que_cumple : ('a -> bool) -> 'a list -> 'a = <fun>


(*existe*)
let existe f l = 
	try 
		primero_que_cumple f l;true
	with
	| _ -> false ;;

- asociado : 'a -> ('a * 'b) list -> 'b

(*asociado*)
let asociado v l =
		if l = [] then raise (Invalid_argument "Lista vacia") else
		let (a,b) = (primero_que_cumple (function (x,y) -> x = v) l) in b;;


(*EJERCICIO 3*)
type 'a arbol_binario = 
      Vacio
    | Nodo of 'a * 'a arbol_binario * 'a arbol_binario;;

(*preorden: 'a arbol_binario -> 'a list *)
let rec preorden = function
	Vacio -> []
	| Nodo (r,Vacio,Vacio) -> [r]
	| Nodo (r,i,d) -> [r]@(preorden i)@(preorden d);;

(*postorden: 'a arbol_binario -> 'a list*)
let rec postorden = function
	Vacio -> []
	| Nodo (r,Vacio,Vacio) -> [r]
	| Nodo (r,i,d) -> (postorden i)@(postorden d)@[r];;

(*inorden: 'a arbol_binario -> 'a list*)
let rec inorden = function
	Vacio -> []
	| Nodo (r,Vacio,Vacio) -> [r]
	| Nodo (r,i,d) -> (inorden i)@[r]@(inorden d);;

NO VAAAAA
(*anchura: 'a arbol_binario -> 'a list*)
let rec anchura = function
	Vacio -> []
	| Nodo (r,Vacio,Vacio) -> [r]
	| Nodo (r,Nodo(),Nodo()) -> r.i  @ r.d anchura i @ anchura d

let a = Nodo(1,Nodo(2,Nodo(4,Vacio,Vacio),Nodo(5,Vacio,Vacio)),Nodo(3,Vacio,Vacio));;




(*EJERCICIO 4*)
type 'a conjunto = Conjunto of 'a list;;

(*es_vacio : 'a conjunto -> bool*)
let es_vacio = function
	Conjunto [] -> true
	| _ -> false;;

(*pertenece : 'a -> 'a conjunto -> bool*)
let rec pertenece v c = 
		match c with
		Conjunto [] -> false
		| Conjunto (h::t) -> if h = v then true else pertenece v (Conjunto t);;

(*agregar : 'a -> 'a conjunto -> 'a conjunto*)
let agregar a c = 
		if pertenece a c then c 
		else let Conjunto l = c in Conjunto (a::l);;

(* conjunto_of_list : 'a list -> 'a conjunto*)
let conjunto_of_list l = 
		let h::t = l in 
			if pertenece h (Conjunto l) 

		res = h::res

Conjunto l;;

NO VAAA
(* suprimir : 'a -> 'a conjunto -> 'a conjunto*)
let suprimir v c = 
	let Conjunto l = c in 
		if pertenece v c then
				let rec aux l2 = 
					match l2 with
					[] -> l2
					| h::t -> if h = v then t else h::aux t
				in l
		else c;;

(* cardinal : 'a conjunto -> int*)
let rec cardinal = function
	Conjunto [] -> 0
	| Conjunto (h::t) -> 1 + cardinal (Conjunto t);;

NO VAAA
(* union : 'a conjunto -> 'a conjunto -> 'a conjunto*)
let rec union a b = 
	let Conjunto l1 = a in
		let Conjunto (h::t) = b in
			if t = [] then []
			else if pertenece h a then union a (Conjunto t)
				 else Conjunto (l1@h@ union a (Conjunto t));;

NO VAAA
(* interseccion : 'a conjunto -> 'a conjunto -> 'a conjunto*)
let interseccion a b =

NO VAAA
(* diferencia : 'a conjunto -> 'a conjunto -> 'a conjunto*)

NO VAAA
(* incluido : 'a conjunto -> 'a conjunto -> bool*)
let rec incluido a b = 
		let Conjunto (h::t) = b in
			if Conjunto h = a then true
			else if t = [] then false 
				 else incluido a (Conjunto t);;

(* igual : 'a conjunto -> 'a conjunto -> bool*)
let rec igual a b = 
	let Conjunto (h1::t1) = a in
		let Conjunto (h2::t2) = b in
			match (t1,t2) with
				([],[]) -> true
			|	([],t2) -> false
			|	(t1,[]) -> false
			|	_		-> h1 = h2 && igual (Conjunto t1) (Conjunto t2);;
 
(* list_of_conjunto : 'a conjunto -> 'a list*)
let list_of_conjunto c = let Conjunto l = c in l;;

(* producto_cartesiano : 'a conjunto -> 'b conjunto -> ('a * 'b) conjunto *)




