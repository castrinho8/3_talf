(* Práctica 1 (Expresiones regulares y autómatas)
Pablo Castro Valiño - pablo.castro1@udc.es
Marcos Chavarria Teijeiro - marcos.chavarria@udc.es
*)

#load "/home/i/pablo.castro1/Desktop/Dropbox/3/TALF Grupo/P1/ocaml-talf/src/talf.cma";;
open Conj;;
open Auto;;
open Ergo;;
open Graf;;

(*EJERCICIO 1. Implementa una función traza_af : Auto.simbolo list -> Auto.af -> bool, que no sólo verifique si una cadena de símbolos terminales es aceptada o no por un autómata finito, sino que además imprima por pantalla todas las configuraciones instantáneas, es decir, todos los pares de la forma (estados actuales, símbolos pendientes), por los que va pasando el proceso de reconocimiento de dicha cadena. Valor de este apartado: 3 puntos.*)

let rec imprimir_configuraciones = function
		[] -> ()
		| h::t -> let formatear = function 
						(Conjunto a,b)	->
								let rec festados sfin = function 
										[] -> sfin ^ "}"
										| h::t -> let Estado s = h in festados (sfin ^ s ^ " ") t 
								in let rec fcadenas cfin = function
										[] -> cfin
										| h::t -> let Terminal s = h in fcadenas (cfin ^ s ^ " ") t 
								in "Estados actuales: " ^ (festados "{ " a) ^ "   Simbolos pendientes: " ^ (fcadenas "" b)
					in print_endline (formatear h); imprimir_configuraciones t;;

let traza_af cadena (Af (_, _, inicial, _, finales) as a) =

   let rec aux = function 
   	
   		[] -> raise (Failure "traza_af")
   		
   	| (Conjunto [], cadena)::tail ->
           (false, (Conjunto [], cadena)::tail)

      | (actuales, [])::tail ->
           (not (es_vacio (interseccion actuales finales)),(actuales, [])::tail)

      | (actuales, simbolo :: t)::tail ->
           aux (((epsilon_cierre (avanza simbolo actuales a) a), t)::(actuales, simbolo :: t)::tail)

   in
      let (est_final,conf) = aux [((epsilon_cierre (Conjunto [inicial]) a), cadena)]
      in imprimir_configuraciones (List.rev conf); est_final
   ;;



(*EJERCICIO 2. Implementa una función af_of_er : Auto.er -> Auto.af, que dada una expresión regular devuelva el autómata finito que acepta exactamente el lenguaje regular denotado por dicha expresión regular. Valor de este apartado: 3 puntos.*)

let camb_nom_est l = function Estado s -> Estado (l ^ s);;

let camb_nom_conjest l = function Conjunto est -> Conjunto (List.map (camb_nom_est l) est);;

let camb_nom_arc l = function Arco_af (a,b,c) -> Arco_af (camb_nom_est l a, camb_nom_est l b, c)

let camb_nom_conjarc l = function Conjunto arc -> Conjunto (List.map (camb_nom_arc l) arc);;

let crear_arcos = function Conjunto lest -> function est ->
	let rec aux lfin = function 
		[] -> Conjunto lfin
	| h::t -> aux (Arco_af (h,est,Terminal "")::lfin) t 
	in aux [] lest;;

let rec af_of_er = function 
		Vacio -> af_of_string "0;;0;;"
		
	|  Constante (Terminal "") -> af_of_string "0;;0;0;"
		
	| 	Constante (Terminal s) -> af_of_string ("0 1;" ^ s ^ " ;0;1;0 1 " ^ s ^ ";")
	
	|  Constante (_) -> raise (Failure "af_of_string") 
	
	| 	Union (a,b) ->  let (Af (est_a,leng_a,ini_a,arc_a,fin_a), Af (est_b,leng_b,ini_b,arc_b,fin_b)) = (af_of_er a, af_of_er b) in
				Af (agregar (Estado "0") (union (camb_nom_conjest "a" est_a) (camb_nom_conjest "b" est_b)), (*Estados dos AF generados por a e b mais un novo estado inicial.*)
				    union leng_a leng_b, (*A union das duas linguaxes.*)
				    Estado "0", (*O novo estado creado*)
				    agregar (Arco_af (Estado "0",camb_nom_est "a" ini_a, Terminal "")) (agregar (Arco_af (Estado "0",camb_nom_est "b" ini_b, Terminal ""))  (union (camb_nom_conjarc "a" arc_a) (camb_nom_conjarc "b" arc_b))),
				    union (camb_nom_conjest "a" fin_a) (camb_nom_conjest "b" fin_b) (*Os estados finais dos AF xerados por a e b *)
				)
				
	| 	Concatenacion (a,b) -> let (Af (est_a,leng_a,ini_a,arc_a,fin_a), Af (est_b,leng_b,ini_b,arc_b,fin_b)) = (af_of_er a, af_of_er b) in
					Af (union (camb_nom_conjest "a" est_a) (camb_nom_conjest "b" est_b),
					    union leng_a leng_b,
					    camb_nom_est "a" ini_a,
					    union (crear_arcos (camb_nom_conjest "a" fin_a) (camb_nom_est "b" ini_b)) (union (camb_nom_conjarc "a" arc_a) (camb_nom_conjarc "b" arc_b)),
					    camb_nom_conjest "b" fin_b
					)
					 
	| 	Repeticion a -> let Af (est_a,leng_a,ini_a,arc_a,fin_a) = af_of_er a in
					Af (agregar (Estado "0") (camb_nom_conjest "a" est_a) , (*Estados dos AF generados por a mais un novo estado inicial.*)
					    leng_a , (*Linguaxe de A*)
					    Estado "0", (*O novo estado creado*)
					    agregar (Arco_af (Estado "0",camb_nom_est "a" ini_a, Terminal "")) (union (crear_arcos (camb_nom_conjest "a" fin_a) (Estado "0")) (camb_nom_conjarc "a" arc_a)),
					    agregar (Estado "0") (camb_nom_conjest "a" fin_a)(*Os estados finais dos AF xerados por a mais o estado inicial creado.*)
					);;


(* EJERCICIO 5 Implementa una función eq_af : Auto.af -> Auto.af -> bool, que dados dos autómatas finitos deterministas devuelve si son equivalentes o no. Valor de este apartado: 3 puntos. *)

let eq_af m1 m2 = let (Af (_,len_a,ini_a,_,_), Af (_,len_b,ini_b,_,_)) = m1,m2 in
		(* Funcion que dados dos automatas finitos deterministas sobre el mismo alfabeto nos dice si son 
		equivalentes. Algoritmo de Moore (Teoria de Automatas y Lenguajes Formales, Dean Kelly. Probl. 2.1 Pag. 93)
		
		Notas sobre la implementación:
			# Se ha considerado que solo es necesario almacenar la primera columna, para ello manejan dos lista, elementos
		    tratados y elementos sin tratar. 
			# La comprobacion de si uno de los los estados del vector es final mientras que el otro no se realiza
		    al tratar el elemento no al agragarlo a la columna.
			*)
		let rec aux m1 m2 lp = function 
	 		[] -> true
		 | (a,b)::t -> 
		 			let es_final a = function 
		 					Af (_,_,_,_,fin_a) -> (if pertenece a fin_a then 1 else 0)
				  	
							(* Devuelve la lista de los que estados desde los que avanza (a,b) con cada una 
							de las letras del alfabeto utilizado. Se supone que los alfabetos son el mismo.*)							
					and sigestados a b m1 m2 = let (Af (_,len,_,arc_a,_), Af (_,_,_,arc_b,_)) = m1,m2 in
								(*Dado una lista de simbolos, devuelve la lista de los siguientes estados a (a,b) 
								para los automatas con deltas definidos por 'arc_a' y 'arc_b'. *)
								let rec aux2 lf = function
									[] -> Conjunto lf
	 						 		| 	h::t -> (*Devuelve el estado siguiente a 'estado' con el simbolo 'letra' para
	 										el automata cuyos arcos son los pasados por parametro.*)
											let rec transicion (Conjunto larc) letra estado = 
												match larc with
													[] -> raise (Failure "sigestados: No es un AFD!!")
				 						 		 | 	Arco_af (est_ini, est_fin, simb )::tail -> 
														if (est_ini = estado) & (simb = letra)
														then est_fin
														else transicion (Conjunto tail) letra estado
											in aux2 ((transicion arc_a h a, transicion arc_b h b)::lf) t
								in let Conjunto lista_simb = len in aux2 [] lista_simb 
							
					in 	let Conjunto l = suprimir (a,b) (union  (Conjunto (((a,b)::t))) (diferencia (sigestados a b m1 m2) (Conjunto lp)) )
						   in ((es_final a m1 + es_final b m2) <> 1) && (aux m1 m2 ((a,b)::lp) l)
		
		in	(igual len_a len_b) && (aux m1 m2 [] [(ini_a,ini_b)]);;


(*EJERCICIO 6. Encuentra dos ejemplos de uso de la función anterior, uno que trabaje sobre dos autómatas equivalentes y otro que trabaje sobre dos autómatas no equivalentes. Valor de este apartado: 1 punto.*)

(*Los automatas siguientes aceptan el lenguaje dado por la expresion regular ab*a y son equivalentes. *)
 let aut_a =  af_of_string "q0 q1 q2 q3; a b; q0; q2; q0 q1 a;q0 q3 b;q1 q2 a;q1 q1 b;q2 q3 a; q2 q3 b;q3 q3 a; q3 q3 b;";;
 let aut_b =  af_of_string "q0 q1 q2 q3 q4; a b; q0; q4; q0 q1 a;q0 q3 b;q1 q4 a;q1 q2 b;q2 q2 b; q2 q4 a;q3 q3 a; q3 q3 b; q4 q3 a; q4 q3 b;";;
 
 (*El automata siguiente acepta el lenguaje dado por la expresión regular a(a|b)*a y no es equivalente a cualquiera de los dos anteriores. *)
 let aut_c =  af_of_string "q0 q1 q2 q3; a b; q0; q2; q0 q1 a;q0 q3 b;q1 q2 a;q1 q1 b;q2 q2 a; q2 q1 b;q3 q3 a; q3 q3 b;";;

 
