(* TALF, PRACTICA 3: Máquinas de Turing
		Pablo Castro Valiño (pablo.castro1@udc.es)
		Marcos Chavarría Teijeiro (marcos.chavarria@udc.es)
*)

(*EJ1*)
let camb_nom_est l = function Estado s -> Estado (l ^ s);;

let camb_nom_conjest l = function Conjunto est -> Conjunto (List.map (camb_nom_est l) est);;

let camb_nom_arc l = function Arco_mt (e1,e2,s1,s2,mov) -> Arco_mt (camb_nom_est l e1, camb_nom_est l e2, s1, s2, mov);;

let camb_nom_conjarc l = function Conjunto arc -> Conjunto (List.map (camb_nom_arc l) arc);;

let crear_arcos dir (Conjunto alf) (Conjunto lest) est =
	let rec aux lfin = function 
		[] -> Conjunto lfin
	| h::t -> let rec aux2 lf = function
					[] -> lf
					| head::tail -> aux2 (Arco_mt(h,est,head,head,dir)::lf) tail
				in aux ((aux2 [] alf) @ lfin) t 
	in aux [] lest;;

let composicion_mt (Mt(est1,alf1,cint1,ini1,reg1,b1,fin1)) (Mt(est2,alf2,cint2,ini2,reg2,b2,fin2)) = 
	if not (igual alf1 alf2)
	then raise (Failure "composicion_mt: Los alfabetos de las maquinas no son iguales.")
	else if not (igual cint1 cint2)
	then raise (Failure "composicion_mt: Los alfabetos de la cinta de las maquinas no son iguales.")
	else 	(Mt(agregar (Estado "0") (union (camb_nom_conjest "a" est1) (camb_nom_conjest "b" est2)),
			alf1,
			cint1,
			camb_nom_est "a" ini1,
		  	union (crear_arcos Izquierda cint1 (Conjunto [Estado "0"]) (camb_nom_est "b" ini2))  (union (crear_arcos Derecha cint1 (camb_nom_conjest "a" fin1) (Estado "0")) (union (camb_nom_conjarc "a" reg1) (camb_nom_conjarc "b" reg2))),
			b1,
			camb_nom_conjest "b" fin2));;



(*EJ2*)
let escribe_blanco = mt_of_string "0 1; a; blanco a; 0; 1;
											 0 1 blanco blanco derecha; 0 1 a blanco derecha;";;

let escribe_a  = mt_of_string "0 1; a; blanco a; 0; 1;
											 0 1 blanco a derecha; 0 1 a a derecha;";;

let izquierda = mt_of_string "0 1; a; blanco a; 0; 1;
										0 1 blanco blanco izquierda; 0 1 a a izquierda;";;

let primer_blanco_a_la_derecha = mt_of_string "0 1 2 3; a; blanco a; 0; 3;
											 0 1 a a derecha; 0 1 blanco blanco derecha; 1 1 a a derecha; 1 2 blanco blanco derecha; 2 3 a a izquierda; 2 3 blanco blanco izquierda;";;
											 
let primer_blanco_a_la_izquierda = mt_of_string "0 1 2 3; a; blanco a; 0; 3;
											 0 1 a a izquierda; 0 1 blanco blanco izquierda; 1 1 a a izquierda; 1 2 blanco blanco izquierda; 2 3 a a derecha; 2 3 blanco blanco derecha;";;

(*EJERCICIO 3 *)
let suma = composicion_mt primer_blanco_a_la_derecha (composicion_mt escribe_a (composicion_mt izquierda (composicion_mt primer_blanco_a_la_derecha (composicion_mt izquierda (composicion_mt escribe_blanco (composicion_mt primer_blanco_a_la_izquierda primer_blanco_a_la_izquierda))))));;

(*EJERCICIO 4*)

let imprimir_cadena pos cadena = 
  let rec  aux n c  = function 
  [] -> c
	| (Terminal "") :: tail -> if (n == 0) 
  										then aux (-1) (c ^ " |blanco|") tail
  										else aux (n-1) (c ^ " blanco") tail
	| (No_terminal "") :: tail -> if (n == 0) 
  										then aux (-1) (c ^ " |blanco|") tail
  										else aux (n-1) (c ^ " blanco") tail
  	| (No_terminal a) :: tail -> if (n == 0) 
  										then aux (-1) (c ^ " |" ^ a ^ "|") tail
  										else aux (n-1) (c ^ " " ^ a) tail
  	| (Terminal a ):: tail -> 	if (n == 0) 
  										then aux (-1) (c ^ " |" ^ a ^ "|") tail
  										else aux (n-1) (c ^ " " ^ a) tail
  in print_endline (aux pos "" cadena);;

let escaner_mt cadena (Mt (_, _, _, inicial, Conjunto delta, _, finales)) =

  if (pertenece inicial finales)
  then (imprimir_cadena 0 cadena; true)
  else let cinta = if cadena = [] then [No_terminal ""] else cadena
       in
       let rec aux = function
           
       ([],[(izq,_,der)],[]) -> (imprimir_cadena (List.length izq) ((List.rev izq) @ der); false)

     | (sigcfs, [], _) -> aux ([], sigcfs, delta)
   
     | (sigcfs, _::cfs, []) -> aux (sigcfs, cfs, delta)
   
     | (sigcfs, (((i::c1, e, s::c2)::_) as cfs),(Arco_mt (e1, e2, s1, s2, Izquierda))::arcos) when e = e1 & s = s1 ->
       if (pertenece e2 finales)         
       then  (imprimir_cadena (List.length c1) ((List.rev (c1)) @ (i::s2::c2)); true)
       else let nc1 = if c1 = [] then [No_terminal ""] else c1
            in (aux ((nc1, e2, i::s2::c2)::sigcfs, cfs, arcos))

     | (sigcfs, (((c1, e, s::c2)::_) as cfs),(Arco_mt (e1, e2, s1, s2, Derecha))::arcos) when e = e1 & s = s1 ->
       if (pertenece e2 finales) 
       then (imprimir_cadena ((List.length c1) + 1) ((List.rev (s2::c1)) @ (c2)); true)
       else let nc2 = if c2 = [] then [No_terminal ""] else c2
            in  (aux ((s2::c1, e2, nc2)::sigcfs, cfs, arcos))

     | (sigcfs, cfs, _::arcos) -> aux (sigcfs, cfs, arcos)
   
     in aux ([], [([No_terminal ""], inicial, cinta)], delta)
;;
